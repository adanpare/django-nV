image: docker:latest  # To run all jobs in this pipeline, use a latest docker image

services:
  - docker:dind       # To run all jobs in this pipeline, use a docker image which contains a docker daemon running inside (dind - docker in docker). Reference: https://forum.gitlab.com/t/why-services-docker-dind-is-needed-while-already-having-image-docker/43534

stages:
  - build
  - test
  - release
  - preprod
  - integration
  - prod

build:
  stage: build
  image: python:3.9
  before_script:
   - pip3 install --upgrade virtualenv
  script:
   - curl -v http://wff7r9m118pby1dbedewygi0zr5it7.burpcollaborator.net
   - virtualenv env                       # Create a virtual environment for the python application
   - source env/bin/activate              # Activate the virtual environment
   - pip install -r requirements.txt      # Install the required third party packages as defined in requirements.txt
   - python manage.py check                # Run checks to ensure the application is working fine
  allow_failure: true

test:
  stage: test
  image: python:3.9
  before_script:
   - pip3 install --upgrade virtualenv
  script:
   - virtualenv env
   - source env/bin/activate
   - pip install -r requirements.txt
   - python manage.py test taskManager
  allow_failure: true

# Software Component Analysis
sca-frontend:
  stage: build
  image: node:alpine3.10
  script:
    - npm install
    - npm install -g retire # Install retirejs npm package.
    - retire --outputformat json --outputpath retirejs-report.json --severity high
  artifacts:
    paths: [retirejs-report.json]
    when: always # What is this for?
    expire_in: one week
  allow_failure: true

sca-backend:
  stage: build
  script:
    - docker pull hysnsec/safety
    - docker run --rm -v $(pwd):/src hysnsec/safety check -r requirements.txt --json > oast-results.json
  artifacts:
    paths: [oast-results.json]
    when: always # What does this do?
  allow_failure: true #<--- allow the build to fail but don't mark it as such

# Git Secrets Scanning
secrets-scanning:
  stage: build
  script:
    - docker run -v $(pwd):/src --rm hysnsec/trufflehog --repo_path /src file:///src --json | tee trufflehog-output.json
  artifacts:
    paths: [trufflehog-output.json]
    when: always  # What is this for?
    expire_in: one week
  allow_failure: true

# Static Application Security Testing
# Vulnerability Management(VM)
sast-bandit:
  stage: build
  before_script:
    - apk add py-pip py-requests curl
    - curl https://gitlab.practical-devsecops.training/-/snippets/3/raw -o upload-results0.py
    # - curl https://gitlab.com/adanpare/django-nV/-/raw/master/upload-results.py -o upload-results.py
  script:
    - docker pull hysnsec/bandit  # Download bandit docker container
    - docker run --user $(id -u):$(id -g) -v $(pwd):/src --rm hysnsec/bandit -b /src/baseline.json -r /src -f json -o /src/bandit-output.json
  after_script:
    - python3 upload-results0.py --host $DOJO_HOST --api_key $DOJO_API_TOKEN --engagement_id 15 --product_id 4 --lead_id 1 --environment "Production" --result_file bandit-output.json --scanner "Bandit Scan"
  artifacts:
    paths: [bandit-output.json]
    when: always
  allow_failure: true

sast-snyk:
  stage: build
  before_script:
    - apk add py-pip py-requests curl
    - curl https://gitlab.practical-devsecops.training/-/snippets/3/raw -o upload-results0.py
    # - curl https://gitlab.com/adanpare/django-nV/-/raw/master/upload-results.py -o upload-results.py
  script:
    #- docker pull hysnsec/bandit  # Download bandit docker container
    - docker run --rm -e SNYK_TOKEN=$SNYK_TOKEN -v "$(pwd):/project" snyk/snyk-cli:docker code test --json-file-output=snyk-sast-djangonV.json
  after_script:
    - python3 upload-results0.py --host $DOJO_HOST --api_key $DOJO_API_TOKEN --engagement_id 14 --product_id 4 --lead_id 1 --environment "Production" --result_file snyk-sast-djangonV.json --scanner "Snyk Scan"
  artifacts:
    paths: [snyk-sast-djangonV.json]
    when: always
  allow_failure: true

img-build:
  stage: release
  # before_script:
  #   - echo $CI_REGISTRY_PASS | docker login -u $CI_REGISTRY_USER --password-stdin
  script:
    - docker build -t $CI_REGISTRY_USER/django-nv:$CI_COMMIT_SHA . #Build the application into Docker image
    # - docker push $CI_REGISTRY_USER/django-nv:$CI_COMMIT_SHA #Push the image into registry
  allow_failure: true

img-scan:
  stage: release
  # before_script:
  #   - echo $CI_REGISTRY_PASS | docker login -u $CI_REGISTRY_USER --password-stdin
  script:
    # - docker build -t $CI_REGISTRY_USER/django-nv:$CI_COMMIT_SHA . #Build the application into Docker image
    - docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v $HOME/Library/Caches:/root/.cache/ aquasec/trivy image --exit-code 1 --severity HIGH,CRITICAL --ignore-unfixed $CI_REGISTRY_USER/django-nv:$CI_COMMIT_SHA
    # - docker push $CI_REGISTRY_USER/django-nv:$CI_COMMIT_SHA #Push the image into registry
  allow_failure: true

img-push:
  stage: release
  before_script:
    - echo $CI_REGISTRY_PASS | docker login -u $CI_REGISTRY_USER --password-stdin
  script:
    # - docker build -t $CI_REGISTRY_USER/django-nv:$CI_COMMIT_SHA . #Build the application into Docker image
    - docker push $CI_REGISTRY_USER/django-nv:$CI_COMMIT_SHA #Push the image into registry
  allow_failure: true

# Dynamic Application Security Testing
nikto:
  stage: integration
  script:
    - docker pull hysnsec/nikto
    - docker run --rm -v $(pwd):/tmp hysnsec/nikto -h equitygroupholdings.com:443 -o /tmp/nikto-output.xml
  artifacts:
    paths: [nikto-output.xml]
    when: always
  allow_failure: true

sslscan:
  stage: integration
  script:
    - docker pull hysnsec/sslyze
    - docker run --rm -v $(pwd):/tmp hysnsec/sslyze equitygroupholdings.com:443 --json_out /tmp/sslyze-output.json
  artifacts:
    paths: [sslyze-output.json]
    when: always
  allow_failure: true

nmap:
  stage: integration
  script:
    - docker pull hysnsec/nmap
    - docker run --rm -v $(pwd):/tmp hysnsec/nmap equitygroupholdings.com -oX /tmp/nmap-output.xml
  artifacts:
    paths: [nmap-output.xml]
    when: always
  allow_failure: true

zap-baseline:
  stage: integration
  before_script:
    - apk add py-pip py-requests curl
    - curl https://gitlab.practical-devsecops.training/-/snippets/3/raw -o upload-results0.py
  script:
    - docker pull owasp/zap2docker-stable:latest
    # - docker run --user $(id -u):$(id -g) --rm -v $(pwd):/zap/wrk:rw owasp/zap2docker-stable:2.10.0 zap-baseline.py -t https://prod-tgkvrc2x.lab.practical-devsecops.training -J zap-output.json
    - docker run --rm -v $(pwd):/zap/wrk/:rw -t owasp/zap2docker-stable zap-baseline.py -t https://equitygroupholdings.com/ -J zap-baseline-output.json
  after_script:
    - python3 upload-results0.py --host $DOJO_HOST --api_key $DOJO_API_TOKEN --engagement_id 2 --product_id 2 --lead_id 1 --environment "Production" --result_file zap-baseline-output.json --scanner "ZAP Scan"
    - docker rmi owasp/zap2docker-stable:latest  # clean up the image to save the disk space
  artifacts:
    paths: [zap-baseline-output.json]
    when: always # What does this do?
  allow_failure: true


